import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import * as Three from 'three'
import {Scene, PerspectiveCamera, WebGLRenderer, BoxGeometry, MeshBasicMaterial, Mesh} from 'three'

// function* boxMuller() {
//   const x = Math.random()
//   const y= Math.random()

//   const z0 = Math.sqrt(-2 * Math.log(x) * Math.cos(2 * Math.PI * y))
//   const z1 = Math.sqrt(-2 * Math.
// }

function gen_normal(mu, sigma) {
  // [-6, 6]
  const value = [...Array(12)].reduce((sum, value) => sum += Math.random(), 0) - 6

  return value / 6
}

function App() {
  const scene = React.useRef(new Scene())
  const camera = React.useRef(new PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000))
  const renderer = React.useRef(new WebGLRenderer())

  const mount = React.useRef()

  const galaxy = React.useRef(new Three.Group())

   /** Init and cleanup */
    React.useEffect(() => {
      console.log("Component Mount")
      renderer.current.setSize(window.innerWidth, window.innerHeight)

      // scene.current.add(cube.current)
      // scene.current.add(cube2.current)
      camera.current.position.z = 200;
      camera.current.position.y = 100;
      camera.current.lookAt(0, 0, 0)
      console.log(camera)

      mount.current.appendChild(renderer.current.domElement)

      const sphere = new Three.SphereGeometry(1)

      for (let i = 0; i < 1000; ++i) {
        const x = gen_normal() * 200 
        const y = gen_normal() * 200 
        const z = gen_normal() * 200

        const material = new Three.MeshBasicMaterial({ color: 0x0000ff })

        const mesh = new Mesh(sphere, material)
        mesh.position.set(x, y, z)

        galaxy.current.add(mesh)
      }

      // Add all meshes
      scene.current.add(new Three.Mesh(new Three.BoxGeometry(), new Three.MeshBasicMaterial({ color: 0xff0000 })))
      scene.current.add(galaxy.current)

      console.log(scene.current)

      // Initial update Call
      update()
    }, [])

    const rotationSpeed = 0.01

    function update() {
      requestAnimationFrame(() => update())

      // cube.current.rotation.x += 0.01
      // cube.current.rotation.y += 0.01
      // galaxy.current.rotation.x += 0.01
      // galaxy.current.rotation.y += 0.01

      galaxy.current.children.forEach(sphere => {
        const x1 = sphere.position.x * Math.cos(rotationSpeed) - sphere.position.z* Math.sin(rotationSpeed)
        const z1 = sphere.position.z * Math.cos(rotationSpeed) + sphere.position.x * Math.sin(rotationSpeed)

        sphere.position.x = x1
        sphere.position.z = z1
      })

      renderer.current.render(scene.current, camera.current)
    }

  return (
    <div>
      <div ref={mount} />
    </div>
  );
}

export default App;
